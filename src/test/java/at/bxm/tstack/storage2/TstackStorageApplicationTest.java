package at.bxm.tstack.storage2;

import static org.hamcrest.MatcherAssert.*;

import org.hamcrest.Matchers;
import org.junit.Test;
import ratpack.test.embed.EmbeddedApp;

public class TstackStorageApplicationTest {

  @Test
  public void test() throws Exception {
    EmbeddedApp.fromServer(new TstackStorageApplication().getServer()).test(client -> {
      assertThat(client.get("/").getStatusCode(), Matchers.is(404));
      assertThat(client.get("/documents").getStatusCode(), Matchers.is(200));
    });
  }
}
