package at.bxm.tstack.storage2;

import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

public class TstackStorageApplication {

  public static void main(String[] args) throws Exception {
   new TstackStorageApplication().getServer().start();
  }

  RatpackServer getServer() throws Exception {
    return RatpackServer.of(spec -> spec
        .serverConfig(ServerConfig.embedded().port(5050))
        .handlers(chain -> chain
            .prefix("documents", documentsChain()
            )
        )
    );
  }

  private static Action<Chain> documentsChain() {
    return chain -> chain
        .get(ctx -> ctx.render("TODO get all"))
        .post(ctx -> ctx.render("TODO create"))
        .get(":id", ctx -> ctx.render("TODO get by id: " + ctx.getPathTokens().get("id")))
        .put(":id", ctx -> ctx.render("TODO save by id: " + ctx.getPathTokens().get("id")));
  }
}
